       IDENTIFICATION DIVISION. 
       PROGRAM-ID. READ-SCORE.
       AUTHOR. MONGKOL.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           88 END-OF-SCORE-FILE VALUE  HIGH-VALUE.
           05 SUBJECT-ID.
              10 TWO-DIGITS PIC X(2).
              10 OTHER-DIGITS   PIC   X(4).
           05 SUBJECT-NAME   PIC   X(43).
           05 CREDIT PIC   X(1).
           05 GRADE PIC   X(2).

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT SCORE-FILE. 
           PERFORM  UNTIL END-OF-SCORE-FILE 
              READ SCORE-FILE 
                 AT END SET END-OF-SCORE-FILE TO TRUE
              END-READ
              IF NOT END-OF-SCORE-FILE THEN
                 DISPLAY "============================================"
                 DISPLAY "ID: " SUBJECT-ID 
                 DISPLAY "SUBJECT NAME: " SUBJECT-NAME
                 DISPLAY "CREDIT: " CREDIT
                 DISPLAY "GRADE: " GRADE
                 DISPLAY "TWO-DIGITS: " TWO-DIGITS
                 DISPLAY "OTHER-DIGITS: " OTHER-DIGITS 
              END-IF 
           END-PERFORM
           CLOSE SCORE-FILE 
           GOBACK 
           .
